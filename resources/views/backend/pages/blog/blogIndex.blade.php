@extends('backend.app')
@section('content')

<div class="container-fluid">
    <div class="card">
        <div class="card-content">
            <a href="{{route('blogs.create')}}" type="button" class="btn bg-gradient-success btn-sm">Blog Oluştur</a>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Bloglar</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
          <table class="table table-sm">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Blog</th>
                <th>Açıklama</th>
                <th style="width:100px">#</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($blogs as $blog )
                <tr>
                    <td>{{$blog->id}}</td>
                    <td>{{$blog->name}}</td>
                    <td>{{Str::limit($blog->description,80)}}</td>
                    <td>
                        <a href="{{route('blogs.edit',$blog->id)}}" type="button" class="btn bg-gradient-primary btn-sm">Düzenle</a>
                        <a href="{{route('blogs.destroy',$blog->id)}}" type="button" class="btn bg-gradient-danger btn-sm">Sil</a>
                    </td>
                  </tr>
                @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
</div>
@endsection
