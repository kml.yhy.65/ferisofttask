@extends('backend.app')
@section('content')
<section class="content">
    <form action="{{route('blogs.store')}}" enctype='multipart/form-data' method="POST">
        @csrf
        @method('post')
    <div class="row">

        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Genel Özellikler</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="inputName">Fotoğraf</label>
                        <input type="file" id="inputName" name="photo" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="inputName">Başlık</label>
                        <input type="text" id="inputName" name="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="inputDescription">Açıklama</label>
                        <textarea id="inputDescription" name="description" class="form-control" rows="4"></textarea>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-md-6">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Özellikler</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="inputStatus">Kategoriler</label>
                        <div class="row">
                            <div class="col-12">
                                <select style="width:100%" id="inputStatus" multiple name="category[]" class="category">
                                    @foreach ($categories as $category)
                                        <option value="{{$category['id']}}">{{$category['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-12">
                        <input type="submit" value="Kaydet" class="btn btn-success float-right">
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    </form>
</section>
@endsection

@push('custom-scripts')
<script>
    $(document).ready(function() {
    $('.category').select2();
});
</script>
@endpush
