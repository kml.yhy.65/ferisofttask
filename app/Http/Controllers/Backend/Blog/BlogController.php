<?php

namespace App\Http\Controllers\Backend\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Blog\BlogStoreRequest;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    // Burada modeller bir defa yükleniyor ve tekrar tekrar kullanılıyor performans açısından çok faydalı
    private Blog $blogModel;
    private Category $categoryModel;
    private BlogCategory $blogCategoryModel;

    function __construct(Blog $blogModel,Category $categoryModel,BlogCategory $blogCategoryModel)
    {
        $this->blogModel=$blogModel;
        $this->categoryModel=$categoryModel;
        $this->blogCategoryModel=$blogCategoryModel;
    }
    /**
     * Blog Index Sayfası
     * @group Blog
     */
    public function index()
    {
        $blogs = $this->blogModel::where('is_active',1)->get();
        return view('backend.pages.blog.blogIndex',compact('blogs'));
    }

    /**
     * Blog Oluşturma Sayfası
     * @group Blog
     */
    public function create()
    {
        $categories = $this->categoryModel::all();
        return view('backend.pages.blog.blogCreate',compact('categories'));
    }

    /**
     * Blog Kayıt
     * @group Blog
     */
    public function store(BlogStoreRequest $request)
    {
        // Validate işlemleri Request sınıfının içerisinde yapılmıştır

        DB::beginTransaction();
        $title = $request->input('title');
        $description = $request->input('description');
        $categories = $request->input('category');

        $blog = new $this->blogModel;


        $blog->name=$title;
        $blog->description=$description;
        $blog->saveOrFail();

        if($request->hasFile('photo')){
            $path = Storage::putFile('blog/'.$blog->id, $request->file('photo'));
            $blog->photo=$path;
            $blog->saveOrFail();
        }

        if(count($categories)>0)
        {
            foreach($categories as $category)
            {
                $this->blogCategoryModel::create(
                    [
                        'blog_id'=>$blog->id,
                        'category_id'=>$category
                    ]
                );
            }
        }
        DB::commit();
        return back()->with('success','Blog Kayıt işlemi başarılı');
    }

    /**
     * Blog Tekil Görüntüleme Sayfası
     * @group Blog
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Blog Tekil Düzenleme Sayfası
     * @group Blog
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Blog Tekil Düzenleme
     * @group Blog
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Blog Silme
     * @group Blog
     */
    public function destroy(string $id)
    {
        //
    }
}
