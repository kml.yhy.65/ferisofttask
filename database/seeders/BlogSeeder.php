<?php

namespace Database\Seeders;

use App\Models\Blog;
use App\Models\BlogCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();
        Blog::truncate();
        $data =[
           [
            "name"=> fake()->title(),
            "description"=> fake()->text(),
            "photo"=>"https://images.unsplash.com/reserve/bOvf94dPRxWu0u3QsPjF_tree.jpg",
            "writer"=>1
            ]
        ];

        foreach($data as $blog){
           $blog = Blog::create(
                [
                    "name"=>$blog["name"],
                    "description"=>$blog["description"],
                    "photo"=>$blog["photo"],
                    "writer"=>$blog["writer"],
                ]
            );
        };

        for($i=0;$i<=2;$i++){
            BlogCategory::create(
                [
                        "blog_id"=>$blog->id,
                        "category_id"=>$i
                ]
            );
        }
        Schema::enableForeignKeyConstraints();
    }
}
