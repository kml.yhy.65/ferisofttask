<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();
        Role::truncate();
        Permission::truncate();
        $roles = [
            [
                "name"=>"Admin",
            ],
            [
                "name"=>"Yazar"
            ]
        ];
        $yazarPermissions =[
            ["name"=>"Blog Create"],
            ["name"=>"Blog Update"],
            ["name"=>"Blog Delete"],
            ["name"=>"Blog View"],
        ];

        foreach($roles as $role){
            Role::create(['name' => $role['name']]);
        }

        $role = Role::findById(1);
        foreach($yazarPermissions as $permission){
            $tmpPermission = Permission::create(['name' =>  $permission['name']]);
            $role->givePermissionTo($tmpPermission);
        };
    }
}
