<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();

        Category::truncate();
        $data =[
           [ "name"=> "Bilim"],
           [ "name"=> "Uzay"],
           [ "name"=> "Teknoloji"],
        ];

        foreach($data as $category){
           $category = Category::create(
                [
                    "name"=>$category["name"]
                ]
            );
        };
        Schema::enableForeignKeyConstraints();
    }
}
